# Power Management System
A script keeping track of the power storage (EU and MJ). It automaticly finds storage connected via wired network. It will toggle power generation when power drops below a defined percent, which is configurable.

To make setup is easy, the script only requires a wired modem connected. If detected, information will be displayed on local monitor and/or termial glasses. Storage can be added on the fly just connecting via wired modem to the network that the computer is on.

Image of example setup:
![](http://i.imgur.com/gVXV5vT.png "Logo Title Text 1")

[Other Images](http://imgur.com/a/qTiWs)


## Requirements
* Minecraft 1.5.2
* ComputerCraft
* OpenPeripherals
* Industrial Craft 2 and/or Thermal Expansion