--################CONFIG########################

--boolean props to control use
ic2 = true
mj = true

--Point in which to begin charging (1 - 99)
chargePercent = 25

--Configure storage devices
--List storage (names received from wired modem) in these tables
--If Auto-detection is enabled these tables can be left blank
ic2Storage = {
	"batbox_0"	
}
mjStorage = {
	
}
--Auto-detect other storage on network?
detection = true

--Configure energy Generation.
--Each item needs the type, direction bundle is attached and wire color
--["type"]["direction"]["color"]
generators = {
	{
		["type"] = "mj",
		["direction"] = "bottom",
		["color"] = 4
	},
	{
		["type"] = "eu",
		["direction"] = "bottom",
		["color"] = 1
	}
}

--###########END OF CONFIG#######################

local function findLocalDevice(deviceType)
	for i, side in pairs(rs.getSides()) do
		if (peripheral.isPresent(side)) then
			if (string.lower(peripheral.getType(side)) == string.lower(deviceType)) then
				return side;
			end
		end
	end
	return nil;
end

local function getStorage()
	maxEU = 0
	maxMJ= 0
	--Grab max storage
	if(ic2) then
		for key,value in ipairs(ic2Storage) do
			if(net.isPresentRemote(value)) then
				maxEU = maxEU + net.callRemote(value,"getCapacity")
			else
				ic2Storage[key] = nil
			end
		end
	end
	if(mj) then
		for key,value in ipairs(mjStorage) do
			if(net.isPresentRemote(value)) then
				maxMJ = maxMJ + net.callRemote(value,"getMaxEnergyStored")
			else
				mjStorage[key] = nil
			end
		end
	end
end

local function findStorage()
	local added = false
	local new = true
	if (mj) then
		for i, remote in ipairs(net.getNamesRemote()) do
			if(string.find(string.lower(remote),"redstone_energy_cell")) then
				for key, value in ipairs(mjStorage) do
					if (remote == value) then
						new = false
					end
				end
				if (new) then 
					table.insert(mjStorage, remote)
					added = true
				else
					new = true
				end
			end
		end
	end

	if (ic2) then
		for i, remote in ipairs(net.getNamesRemote()) do
			if(string.find(string.lower(remote), "batbox")) then
				for key, value in ipairs(ic2Storage) do
					if (remote == value) then
						new = false
					end
				end
				if (new) then 
					table.insert(ic2Storage, remote)
					added = true
				else
					new = true
				end
			end
		end
	end
	return added
end

local function setGeneration(strType, boolean, list)
	local redstoneValue = {}

	for key, value in pairs(list) do
		if (value["type"] == strType) then
			if(redstoneValue[value["direction"]] == nil)then
				redstoneValue[value["direction"]] = redstone.getBundledOutput(value["direction"])
			end
			if (boolean) then
				if (not colors.test(redstoneValue[value["direction"]],value["color"])) then
					redstoneValue[value["direction"]] = redstoneValue[value["direction"]] + value["color"]
				end
			else
				if (colors.test(redstoneValue[value["direction"]],value["color"])) then
					redstoneValue[value["direction"]] = redstoneValue[value["direction"]] - value["color"]
				end
			end
		end
	end
	-- Set redstone bundle
	for key, value in pairs(redstoneValue) do
		redstone.setBundledOutput(key, value)
	end
end
local function init()
	--Check for monitor
	if(findLocalDevice("monitor")) then
		term.clear()
		term.write("monitor connected")
		term.redirect(peripheral.wrap(findLocalDevice("monitor")))
	end

	--Prepare the screen
	term.clear()
	term.setCursorPos(1,1)
	term.write("Power Management")
	term.setCursorPos(1,2)
	term.write("EU = NA")
	term.setCursorPos(1,3)
	term.write("0/0")
	term.setCursorPos(1,4)
	term.write("MJ = NA")
	term.setCursorPos(1,5)
	term.write("0/0")
	term.setCursorPos(1,6)

	if(not (ic2 or mj))then
		term.write("Please choose sources in the config")
		shell.exit()
	end

	--Variables to storage amounts
	storedEU = 0
	maxEU = 0
	storedMJ = 0
	maxMJ = 0
	euPercent = 0
	mjPercent = 0
	--Other Vars
	bridge = nil
	net = nil

	--Find network
	if (findLocalDevice("modem") == nil) then
		term.write("Cannot find Network")
		shell.exit()
	else
		net = peripheral.wrap(findLocalDevice("modem"))
	end

	if(detection) then
		findStorage()
	end

	getStorage()

	--Dectect Bridge
	if (findLocalDevice("terminal_glasses_bridge")) then
		bridge  = peripheral.wrap(findLocalDevice("terminal_glasses_bridge"))
	end
	if (bridge) then
	--Prepare Glasses
		bridge.clear()
		bridge.addText(1,1,"Power Management",colors.gray)
		euText = bridge.addText(1,10, "EU = NA", colors.gray)
		bridge.addBox(3, 20, 100, 5, colors.gray, 100)
		euBar = bridge.addBox(3, 20, 0, 5, colors.red, 100)
		euBar.setZIndex(4) 
		mjText = bridge.addText(1,28, "MJ = NA", colors.gray)
		bridge.addBox(3, 38, 100, 5, colors.gray, 100)
		mjBar = bridge.addBox(3, 38, 0, 5, colors.red, 100)
		mjBar.setZIndex(4)
	end
end
local function main()
	storedMJ = 0
	storedEU = 0
	if(ic2) then
		--Get amount of stored EU
		for key,value in ipairs(ic2Storage) do
			if(net.isPresentRemote(value)) then
				storedEU = storedEU + net.callRemote(value,"getStored")
			else 
				getStorage()
			end
		end
	end
	if(mj) then
		--Get amount of stored MJ
		for key,value in ipairs(mjStorage) do
			if(net.isPresentRemote(value)) then
				storedMJ = storedMJ + net.callRemote(value,"getEnergyStored")
			else 
				getStorage()
			end
		end
	end
	euPercent = math.floor((storedEU / maxEU) * 100)
	mjPercent = math.floor((storedMJ / maxMJ) * 100)
	--control status
	if(ic2) then
		if (euPercent < chargePercent) then
			setGeneration("eu",true,generators)
		elseif((euPercent >= 100) or (maxEU == 0))then
			setGeneration("eu",false,generators)
		end
		--update screen
		term.setCursorPos(1,2)
		term.clearLine()
		term.write("EU = " .. euPercent .. "%")
		term.setCursorPos(1,3)

		--quick fix for IC2 output buffer
		if (storedEU > maxEU) then
			storedEU = maxEU
		end

		term.write(storedEU .. "/" .. maxEU)
	end

	if (mj) then
		if (mjPercent < chargePercent) then
			setGeneration("mj",true, generators)
		elseif((mjPercent >= 100) or (maxMJ == 0)) then
			setGeneration("mj", false, generators)
		end

		term.setCursorPos(1,4)
		term.clearLine()
		term.write("MJ = " .. mjPercent .. "%")
		term.setCursorPos(1,5)
		term.write(math.floor(storedMJ) .. "/" .. maxMJ)
	end
	term.setCursorPos(1,6)

	if(bridge) then
		--paint glasses
		if(ic2) then
			euText.setText("EU = " .. euPercent .. "%")
			euBar.setWidth(euPercent)
		end
		if(mj) then
			mjText.setText("MJ = " .. mjPercent .. "%")
			mjBar.setWidth(mjPercent)
		end	
	end
	sleep(1)
end

local function eventListener()
	sleep(0)
	p1, p2 = os.pullEventRaw()
	if p1 == "terminate" then
		quit = true
	end
end

local function autoDetect()
	local found = findStorage()
	if(found) then
		getStorage()
	end
end

quit = false
init()

while true do
	parallel.waitForAny(eventListener, main)

 	if quit then
	    for i, value in pairs(rs.getSides()) do
			rs.setBundledOutput(value, 0)
		end
		term.clear()
		term.restore()
		term.clear()
		term.setCursorPos(1,1)
		if (findLocalDevice("terminal_glasses_bridge")) then
			peripheral.call(findLocalDevice("terminal_glasses_bridge"),"clear")
		end
		print("Terminated")
	    break
	end
	if(detection) then
		autoDetect()
	end

	sleep(0)
end